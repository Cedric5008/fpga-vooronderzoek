/** Copyright (c) 2020-2021 LUMC. This file is created by Cedric de Wijs 
 * This file is (at your option) licensed as GPL version 2 or any later version
 * or LGPL version 2 or any later version. See the file LICENSE-GPL.txt and 
 * LICENSE-LGPL.txt for details. This file is distributed in the hope that it 
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. In no event shall
 * the copyright owner or contributors be liable for any damages however caused */

`include "spi.v"

module spi_testbench();
	reg spiclk, cs, mosi;
	wire [15:0] data;
	spi dut(spiclk, cs, mosi, data);
	
	initial begin
		$dumpfile("spi_test.vcd"); 
		$dumpvars(1, spiclk);
		spiclk = 0;
		$dumpvars(1, cs);
		cs = 0;
		$dumpvars(1, mosi);
		mosi =0;
		$dumpvars(1, data);
	end

	initial begin
		#10000  //after this delay...
		$error("fail"); //spits out an error statement on the console complete with the line and clockcycle of the error
		$finish; //end simulation with exit code 0
		//$fatal(1); //ends the simulator with exit code 1
	end
	
	initial begin
		#10
		mosi = 1;
		#10
		spiclk = 1;
		#10
		spiclk = 0;
		#10
		spiclk = 1;
		#10
		spiclk = 0;
		#10
		spiclk = 1;
		#10
		cs = 1;
		#10
		spiclk = 0;
		#10
		spiclk = 1;
		#10
		spiclk = 0;
		#10
		spiclk = 1;
		#10
		spiclk = 0;
		#10
		spiclk = 1;
		#10
		spiclk = 0;
		#10
		spiclk = 1;
		#10
		spiclk = 0;
	end
endmodule
