# Copyright (c) 2020-2021 LUMC. This file is created by Cedric de Wijs 
# This file is (at your option) licensed as GPL version 2 or any later version
# or LGPL version 2 or any later version. See the file LICENSE-GPL.txt and 
# LICENSE-LGPL.txt for details. This file is distributed in the hope that it 
# will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. In no event shall
# the copyright owner or contributors be liable for any damages however caused

#!/bin/bash

iverilog -Wall spi_test.v
vvp a.out 
# echo $? # to figure out what the exit code of vpp is
# gtkwave spi_test.vcd -a test.gtkw #uncomment this for showing the waveforms from the simulation
