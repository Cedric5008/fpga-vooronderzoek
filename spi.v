/** Copyright (c) 2020-2021 LUMC. This file is created by Cedric de Wijs 
 * This file is (at your option) licensed as GPL version 2 or any later version
 * or LGPL version 2 or any later version. See the file LICENSE-GPL.txt and 
 * LICENSE-LGPL.txt for details. This file is distributed in the hope that it 
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. In no event shall
 * the copyright owner or contributors be liable for any damages however caused */

module spi(input spiclk, input cs, input mosi, output[15:0] data);
	reg [15:0] data_shift;
	initial begin
		data_shift = 0;
	end

	always @(posedge spiclk) begin
		if (cs) begin
 			data_shift[15:0] <= {mosi,data_shift[15:1]};
 			$display ("data_shift: %0d ",data_shift);
 		end
	end
	
	assign data = data_shift;
endmodule
